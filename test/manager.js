const fs = require('fs-extra');
const util = require('util');
const path = require('path');
const readDir = util.promisify(fs.readdir);
const readFile = util.promisify(fs.readFile);
const assert = require('assert');
const LoggerManager = require('./../src/manager');

describe('LoggerManager', () => {
  after(() => fs.removeSync(LoggerManager.defaultConfiguration.dir));

  it('default configuration', () => {
    const config = LoggerManager.defaultConfiguration;

    assert.ok(typeof config === 'object');
    assert.ok(config.dir);
    assert.ok(config.level);

    assert.ok(config.archive);
    assert.ok(config.archive.maxFilesCountByLevel);
    assert.ok(config.archive.maxFilesSize);

    assert.ok(config.files);
    ['info', 'error', 'debug', 'process'].forEach(level => {
      assert.ok(config.files[level]);
      assert.ok(config.files[level].name);
      assert.ok(typeof config.files[level].isEnabled === 'boolean');
    });

    assert.ok(config.logStash);
    assert.ok(typeof config.logStash.isEnabled === 'boolean');
    assert.ok(config.logStash.port);
    assert.ok(config.logStash.host);
    assert.ok(config.logStash.nodeName);
  });

  it('throws Error for invalid "module" param', () => {
    try {
      new LoggerManager().getInstance(null);
    } catch (err) {
      assert.ok(err instanceof Error);
      assert.ok(/module/g.test(err.message));
      return;
    }

    throw new Error('Must be gone to catch');
  });

  it('successfully get logger instance', () => {
    return new LoggerManager().getInstance(module);
  });

  it('successfully write logs', async () => {
    const log = new LoggerManager().getInstance(module);
    const files = [
      LoggerManager.defaultConfiguration.files.info.name,
      LoggerManager.defaultConfiguration.files.error.name,
      LoggerManager.defaultConfiguration.files.debug.name,
      LoggerManager.defaultConfiguration.files.process.name
    ];

    log.info('test');
    log.error('test');
    log.debug('test');
    log.process('test');

    await delay();
    const data = await readDir(LoggerManager.defaultConfiguration.dir);

    assert.ok(Array.isArray(data));
    assert.ok(data.length === 4);
    files.forEach(file => assert.ok(data.includes(file)));

    const promises = files.map(async fileName => {
      const file = await readFile(path.join(LoggerManager.defaultConfiguration.dir, fileName));
      const json = JSON.parse(file);
      assert.ok(typeof json === 'object');
      assert.ok(json.message === 'test');
    });

    return await Promise.all(promises);
  });
});

async function delay() {
  return new Promise(resolve => setTimeout(() => resolve(), 300));
}
