const LoggerManager = require('./manager');

module.exports = config => new LoggerManager(config);
