const LoggerTransports = require('./transports');

class Logger {
  constructor(config, label) {
    this.config = config;
    this._trasports = new LoggerTransports(config, label).getAllTransports();
  }

  debug(...args) {
    return this._callByLogLevel('debug', args);
  }

  error(...args) {
    return this._callByLogLevel('error', args);
  }

  info(...args) {
    return this._callByLogLevel('info', args);
  }

  process(...args) {
    return this._callByLogLevel('process', args);
  }

  _callByLogLevel(level, args) {
    const normalizedArgs = this._getNormalizedArguments(args);

    this._trasports
      .filter(t => t.level === level)
      .forEach(t => {
        const loggerLevel = ['debug', 'error', 'info'].includes(level) ? level : this.config.level;
        t.logger[loggerLevel].apply(null, normalizedArgs);
      });
  }

  _getNormalizedArguments(args) {
    const normalized = [];

    args.forEach(argument => {
      if (argument instanceof Error) {
        if (typeof argument.toLogObject === 'function') {
          normalized.push(argument.toLogObject());
        } else {
          normalized.push({
            message: argument.message,
            stack: argument.stack.split('\n').map(trace => trace.trim())
          });
        }
      } else {
        normalized.push(argument);
      }
    });

    return normalized;
  }
}

module.exports = Logger;
