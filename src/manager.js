const _ = require('lodash');
const path = require('path');
const Logger = require('./logger');

class LoggerManager {
  constructor(config) {
    this.config = _.defaults(config || {}, LoggerManager.defaultConfiguration);
  }

  getInstance(module) {
    if (!module) {
      throw new Error('Param "module" must be specified');
    }
    return new Logger(this.config, getLabel(module));
  }
}

LoggerManager.defaultConfiguration = {
  level: 'debug',
  dir: path.join(__dirname, '..', 'logs'),
  archive: {
    maxFilesCountByLevel: 10,
    maxFilesSize: 52428800
  },
  files: {
    debug: {
      isEnabled: true,
      name: 'debug.log'
    },
    error: {
      isEnabled: true,
      name: 'error.log'
    },
    info: {
      isEnabled: true,
      name: 'info.log'
    },
    process: {
      isEnabled: true,
      name: 'process.log'
    }
  },
  logStash: {
    isEnabled: false,
    port: 28777,
    host: '127.0.0.1',
    nodeName: 'test-log-stash-node'
  }
};

module.exports = LoggerManager;

function getLabel(module) {
  return module.filename.split('/').slice(-3).join('/');
}
