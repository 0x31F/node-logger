const fs = require('fs');
const winston = require('winston');
const helpers = require('./helpers');

require('winston-logstash');

class LoggerTransports {
  constructor(config, label) {
    this.config = config;
    this.label = label;
    this._handleConfiguration();
  }

  getAllTransports() {
    let transports = [];

    transports = transports.concat(this._getDefaultConsoleLoggers());
    transports = transports.concat(this._getDefaultFileLoggers());

    if (this.config.logStash.isEnabled) {
      transports = transports.concat(this._getLogStashLoggers());
    }

    return transports;
  }

  _getDefaultConsoleLoggers() {
    const consoleOptions = helpers.getDefaultLoggerSettings(this.label, this.config.level);
    const console = new winston.transports.Console(consoleOptions);

    return [
      {
        level: this.config.level,
        logger: new winston.Logger({transports: [console]})
      }
    ];
  }

  _getDefaultFileLoggers() {
    const transports = [];

    if (this.config.files.debug.isEnabled) {
      transports.push(this._getFileTransport('debug'));
    }

    if (this.config.files.error.isEnabled) {
      transports.push(this._getFileTransport('error'));
    }

    if (this.config.files.info.isEnabled) {
      transports.push(this._getFileTransport('info'));
    }

    if (this.config.files.process.isEnabled) {
      transports.push(this._getFileTransport('process'));
    }

    return transports;
  }

  _getLogStashLoggers() {
    const logStash = this.config.logStash;
    const debug = new winston.transports.Logstash(helpers.getLogStashLoggerSettings(logStash, 'debug'));
    const error = new winston.transports.Logstash(helpers.getLogStashLoggerSettings(logStash, 'error'));
    const info = new winston.transports.Logstash(helpers.getLogStashLoggerSettings(logStash, 'info'));

    return [
      {level: 'debug', logger: new winston.Logger({transports: [debug]})},
      {level: 'error', logger: new winston.Logger({transports: [error]})},
      {level: 'info', logger: new winston.Logger({transports: [info]})}
    ];
  }

  _getFileTransport(level) {
    const transport = helpers.getFileTransportByLogLevel(this.config, this.config.files[level].name, this.label);
    return {level, logger: new winston.Logger({transports: [transport]})};
  }

  _handleConfiguration() {
    const dir = this.config.dir;

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
  }
}

module.exports = LoggerTransports;
