const winston = require('winston');

module.exports = {
  getDefaultLoggerSettings(label, level) {
    return {
      colorize: true,
      timestamp: true,
      prettyPrint: true,
      level,
      label
    };
  },

  getLogStashLoggerSettings(logStash, level) {
    return {
      port: logStash.port || '28777',
      host: logStash.host || 'localhost',
      node_name: logStash.node || 'log-stash-node',
      max_connect_retries: 1,
      level
    };
  },

  getFileTransportByLogLevel(config, fileName, label) {
    const consoleOptions = this.getDefaultLoggerSettings(label, config.level);
    const fileOptions = Object.assign({}, consoleOptions, {
      dirname: config.dir,
      filename: fileName,
      level: config.level,
      maxFiles: config.archive.maxFilesCountByLevel,
      maxsize: config.archive.maxFilesSize
    });

    return new winston.transports.File(fileOptions);
  }
};
